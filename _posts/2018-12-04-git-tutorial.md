---
title:  Git For Data Scientists
header:

    overlay_image: /assets/images/empty_auditorium.jpg
---

## Reproducible Research

Dealing with academic code, texts, data analyses and data itself is tough. Code and data can change rapidly, can be exchanged, require review and feedback, maintenance and care. Eventually, code and/or data will be the basis of a publication.

Handling code and data in an efficient way has become one hallmark skill that a scientist in the 21st must master when quantitativeresearch is at the heart of her or his activities. Any failure can rapidly lead to results which cannot be reproduced and hence do not lend themselves for publication.

![]({{site.baseurl}}/assets/openscienceMOOC_principles_large.jpg){:class="img-fluid"}{:style="max-width: 50%"}

The figure above by [OpenScienceMOOC](https://github.com/OpenScienceMOOC/Module-5-Open-Research-Software-and-Open-Source/blob/master/content_development/images/simple_rules.png) (CC0 licensed) illustrates how central version control is as 5 out 10 points involve it directly. Point 4 explicitely states "Version Control all Custom Scripts". For this reason (and many others), we are organizing a course which introduces one state-of-the-art version control system called `git`.

## Git For Data Scientists

"Git for Data Scientists" is a course organized by the MLC Dresden. It will take place on December 20, 2018, at 1.30pm in Görgesbau Room 226. 

The tutorial will last a maximum of 4.5 hours and will be fully hands-on on your personal laptop. It is free of charge. Please consult the [course page](https://indico.mpi-cbg.de/e/mlc-git-2018) for further details.

Interested participants can register at [indico.mpi-cbg.de/e/mlc-git-2018](https://indico.mpi-cbg.de/e/mlc-git-2018). 
