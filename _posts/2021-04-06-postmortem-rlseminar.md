---
title:  Recap of our MLC Seminar on Reinforcement Learning
excerpt: ""
## excerpt: A Short Recap
header:
    overlay_image: /assets/images/daniel-gimbel--ship-on-rhine-near-bonn-5adI2z8m1WE-unsplash_1600px.jpg
    caption: "Photo credit: [**unsplash.com**](https://unsplash.com/photos/5adI2z8m1WE)"
categories:
  - Seminars
---

On April 6, 2021, **Fabian Hart (TU Dresden)** presented his **Introduction to Reinforcement Learning** to our community. If you weren't able to attend, his slide deck is available on [figshare.com:441d5dd9a7342bb82fde](https://figshare.com/s/441d5dd9a7342bb82fde). 

In a nutshell, Fabian introduced reinforcement learning, so the optimization of an agent's actions over time given a specific reward. Fabian chose to start from his application as his focus is to design an automated system to navigate freighters on the Lower Rhine river in Germany. Among the important tasks to approach with this technique are: trajectory planning, overtake maneuver decision making, ship-following mode, safety distances, ...

The agent, a ship in Fabian's case, is exposed to a bag of input information it has to account for while making decisions. These can be of external nature such as the river geometry ahead, the river flow dynamics, or the flow depth. They can also be more internal to the vessel itself, such as the engine dynamics, hydrodynamic effects reacting to the rudder, [ship squat dependent on speed](https://en.wikipedia.org/wiki/Squat_effect) to name a few. From the many actions such a vessel has to perform, Fabian selected the Longitudinal Control as the field he introduced reinforcement learning. Make sure to dive into his [slides on figshare](https://figshare.com/s/441d5dd9a7342bb82fde) to get a closer look.

The seminar was well attended reaching 60 participants logged into zoom. It was nice to see colleagues from Leipzig there too. The question and answer session sampled from this crowd rather effectively. The interactive notes on [hackmd.io](https://hackmd.io) captured nine questions which in turn led to sub-discussions each. The colloquium that developed was super interesting even for people not familiar with reinforcement learning. Community members also shared interesting papers that e.g. investigate the interpretability of such agents. Feel free to have a look at [the show notes]({{site.baseurl}}/assets/20210406-seminar-fabianhart-rl-hackmd-notes.md).
