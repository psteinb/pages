---
title:  Deep Learning in Medical and Biological Image and Video Segmentation
header:

    overlay_image: /assets/images/empty_auditorium.jpg
---

# Second MLC Meeting on November 8

Our second seminar about machine learning
will be focused on deep learning in medical and biological image and video
segmentation. Therefore, we are happy to welcome Walter de Back from the
Institute for Medical Informatics and Biometry who is an experienced scientist
in this field. Walter will lead the half-day event including an introduction and
small lecture as well as a hand on tutorial. The event will be suitable for
beginners and advanced users in deep learning. 

When: 8th of November from 9 am to 2 pm (including 1 hour lunch break)

Where: Toepler-Bau of TU Dresden, Mommsenstraße 12 in room TOE 203

## Abstract of the seminar

Deep learning has a great potential for biomedical image analysis. In
particular, convolutional neural networks are an important class of machine
learning techniques that can be trained to classify, detect, localize, and
segment objects or abnormalities in different image modalities by learning to
extract relevant image features. Recent studies have shown that deep
learning-based systems can outperform human radiologists in accuracy on a
variety of tasks, at only a fraction of the time and cost. 
In this seminar, I will provide a gentle introduction to convolutional neural
networks and their application to biomedical image analysis tasks, highlighting
both their strengths and limitations. In addition, we will have a hands-on
session training a neural network to segment biomedical images, based on
[keras](https://keras.io/).

## About Walter de Back

Walter de Back has an MSc in Artificial Intelligence from Utrecht University
(NL) and obtained a PhD in Computational Biology from TU Dresden for studies on
pattern formation in tissues using multi-scale computational modeling. Walter
now works as a postdoc data scientist at the Faculty of Medicine (TU Dresden)
where he uses deep learning in a number of projects including cell segmentation
in live cell microscopy, dental age estimation from panoramic radiographs, and
tumor tissue classification based on mass spectrometry imaging data. More
information on: [https://wdeback.gitlab.io/](https://wdeback.gitlab.io/)
